#include <Windows.h>
#include <iostream>

HINSTANCE l_hInstance;
int l_nCmdShow;
HWND l_mainWnd;

int FullColor = -1;
int RowStart = -1;
int CoLStart = -1;
int WidthF = -1;
int HeightF = -1;
HBITMAP l_hBitMaP = NULL;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL InitAppClass();
BOOL InitWindow();
WPARAM StartMessageLoop();

bool loadImage()
{
	l_hBitMaP = (HBITMAP)LoadImage(NULL, L".\\LR1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION);

	if (!l_hBitMaP)
		return false;

	BITMAP bm;
	GetObject(l_hBitMaP, sizeof(BITMAP), &bm);

	uint8_t* data = (uint8_t*)bm.bmBits;

	int minrange = -1;
	int maxrange = -1;
	int mincolumn = -1;
	int maxcolumn = -1;

	for (int row = 0; row < bm.bmHeight; ++row)
	{

		int col_min = -1;
		int col_max = -1;

		for (int col = 0; col < bm.bmWidth; ++col)
		{
			uint8_t& r = data[row * bm.bmWidth * 3 + col * 3];
			uint8_t& g = data[row * bm.bmWidth * 3 + col * 3 + 1];
			uint8_t& b = data[row * bm.bmWidth * 3 + col * 3 + 2];

			int rgb = ((int)b << 16) | ((int)g << 8) | (int)r;

			if (FullColor == -1)
			{
				FullColor = rgb;
			}

			const bool is_fill = rgb == FullColor;

			if (!is_fill)
			{
				if (col_min < 0) col_min = col;
			}
			else
			{
				if (col_min > 0 && col_max < 0) col_max = col - 1;

				r = g = b = 255;
			}
		}

		if (col_min > 0)
		{

			if (minrange < 0) minrange = row;


			const int col_d = col_max - col_min;
			if (col_d > maxcolumn - mincolumn)
			{
				mincolumn = col_min;
				maxcolumn = col_max;
			}
		}
		else
		{
			if (minrange > 0 && maxrange < 0) maxrange = row - 1;
		}
	}

	WidthF = maxcolumn - mincolumn;
	HeightF = maxrange - minrange;
	RowStart = minrange;
	CoLStart = mincolumn;

	return true;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	l_hInstance = hInstance;
	l_nCmdShow = nCmdShow;

	if (!loadImage())
		return 0;
	if (!InitAppClass())
		return 0;
	if (!InitWindow())
		return 0;

	return StartMessageLoop();
}

BOOL InitAppClass()
{
	ATOM class_id;
	WNDCLASS wc;
	memset(&wc, 0, sizeof(wc));
	wc.lpszMenuName = NULL;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = (WNDPROC)WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = l_hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.lpszClassName = L"LR_Mazlova";

	class_id = RegisterClass(&wc);
	if (class_id != 0)
		return TRUE;
	return FALSE;

}

BOOL InitWindow()
{
	l_mainWnd = CreateWindow(L"LR_Mazlova", L"Praktika_Mazlova", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
		600, 600, 0, 0, l_hInstance, 0);

	if (!l_mainWnd)
		return FALSE;
	ShowWindow(l_mainWnd, l_nCmdShow);
	UpdateWindow(l_mainWnd);
	return TRUE;
}

WPARAM StartMessageLoop()
{
	MSG msg;
	while (1)
	{
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				break;
			DispatchMessage(&msg);
		}
	}
	return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		DeleteObject(l_hBitMaP);
		return 0;
	}

	case WM_PAINT:
	{
		PAINTSTRUCT     ps;
		HDC             hdc;
		BITMAP          bitmap;
		HDC             h_d_c;
		HGDIOBJ         BiT_Map;
		RECT r;

		GetClientRect(hWnd, &r);

		int some_x = r.left - WidthF;
		int some_y = r.bottom - HeightF;

		if (some_x < 0) some_x = 0;
		if (some_y < 0) some_y = 0;

		hdc = BeginPaint(hWnd, &ps);

		h_d_c = CreateCompatibleDC(hdc);
		BiT_Map = SelectObject(h_d_c, l_hBitMaP);

		GetObject(l_hBitMaP, sizeof(bitmap), &bitmap);

		if (r.right > WidthF && r.bottom > HeightF)
		{
			BitBlt(hdc, some_x, some_y, r.right, r.bottom, h_d_c, CoLStart, bitmap.bmHeight - (RowStart + HeightF), SRCCOPY);
		}
		else
		{

			const int l = (r.right < r.bottom) ? r.right : r.bottom;
			int x_start, y_start;
			if (l == r.right)
			{
				x_start = some_x;
				y_start = r.bottom - l;
			}
			else
			{
				x_start = r.right / 2 - l / 2;
				y_start = some_y;
			}

			StretchBlt(hdc, x_start, y_start, l, l, h_d_c, CoLStart, bitmap.bmHeight - (RowStart + HeightF), WidthF, HeightF, SRCCOPY);
		}

		SelectObject(h_d_c, BiT_Map);
		DeleteDC(h_d_c);

		EndPaint(hWnd, &ps);
		break;
	}
	default:
		break;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}